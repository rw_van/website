+++
title = "Redox em Ação"
+++

## Redox OS reproduzindo a intro da temporada 10 de Stargate SG-1
<iframe width="560" height="315" src="https://www.youtube.com/embed/3cPekY4c9Hc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Redox OS executando Mesa3D Gears com LLVMpipe
<iframe width="560" height="315" src="https://www.youtube.com/embed/ADSvEA_YY7E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## System76 Galago Pro (galp3-c)
<img class="img-responsive" src="/img/hardware/system76-galp3-c.jpg"/>

## Panasonic Toughbook CF-18
<img class="img-responsive" src="/img/hardware/panasonic-toughbook-cf18.png"/>

## ASUS eeePC 900
<img class="img-responsive" src="/img/hardware/asus-eepc-900.png"/>

## ThinkPad T420
<img class="img-responsive" src="/img/hardware/thinkpad-t420.png"/>

## ThinkPad T520, ThinkPad P50 and Asus P7P55D-E Pro Desktop
<img class="img-responsive" src="/img/hardware/T520-P50-Asus-Desktop.jpg"/>
