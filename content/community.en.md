+++
title = "Community"
+++

This page attempts to explain how the Redox OS community is organised and help you navigate it.


<a id="chat"></a>
## [Chat](https://matrix.to/#/#redox:matrix.org)

Matrix is the official way to talk with Redox OS team/community.

<a id="gitlab"></a>
## [GitLab](https://gitlab.redox-os.org/redox-os/redox)

A bit more formal way of communication with fellow Redox devs, but a little less quick and convenient like the chat. Submit an issue when you run into problems compiling, testing, or just would like to discuss a certain topic, be it features, code style, code inconsistencies, minor changes and fixes, etc.

<a id="reddit"></a>
## [Reddit](https://www.reddit.com/r/Redox/)

If you want a quick look at what's going on and talk about it.

[reddit.com/r/rust](https://www.reddit.com/r/rust) for related Rust news and discussions.

<a id="mastodon"></a>
## [Mastodon](https://fosstodon.org/@redox)

News and related content.

<a id="chat"></a>
## [Mattermost](https://chat.redox-os.org)

Until recently, we have been using a Mattermost chat server, which we are currently maintaining for historical purposes. Contact us on Redox OS/General if you need access to the Mattermost server.

<a id="forum"></a>
## [Forum](https://discourse.redox-os.org/)

This is our historical forum with old/classic questions, it's inactive and must be used for historical purposes, if you have a question, send on Matrix chat.

<a id="note"></a>
### Note

Community outreach is an important part of Redox's success. If more people know about Redox, then more contributors are likely to step in, and everyone can benefit from their added expertise. You can make a difference by writing articles, talking to fellow OS enthusiasts, or looking for communities that would be interested in knowing more about Redox.
