+++
title = "Doações"
+++

## Redox OS Organização Sem Fins Lucrativos

O Redox OS tem uma organização sem fins lucrativos no Colorado (EUA) que gerencia as doações, essa organização sem fins lucrativos atualmente não está isenta de impostos, mas está trabalhando para adquirir o status `501(c)(3)`.

As doações da organização serão usadas conforme as decisões da mesa de diretores.

Você pode doar para o Redox OS das seguintes formas:

 - [Patreon](https://www.patreon.com/redox_os)
 - [Donorbox](https://donorbox.org/redox-os)
 - Para doações maiores que 1000 dólares ou mais opções de doações, por favor entre em contato no endereço de e-mail:
   donate@redox-os.org

## Jeremy Soller

Jeremy Soller é o criador, mantenedor e desenvolvedor chefe do Redox OS.

Doações para Jeremy Soller são tratadas como presente taxável e serão usadas conforme sua vontade.

Você pode doar para Jeremy Soller das seguintes formas:

- [Liberapay](https://liberapay.com/redox_os)
- [Paypal](https://www.paypal.me/redoxos)
- Jeremy Soller não aceita doações em Bitcoin ou Ethereum, não envie nada para os endereços anteriores, pois elas não serão recebidas.

Os seguintes Patreons doaram 4 dólares ou mais para Jeremy Soller desenvolver o Redox OS:
{{% partial "donors/jackpot51.html" %}}
