+++
title = "Donate"
+++

## Redox OS Nonprofit

Redox OS has a Colorado (US) incorporated nonprofit that manages donations. This
nonprofit is not currently tax-exempt, but is working on acquiring `501(c)(3)`
status. Donations to the Redox OS nonprofit will be used as determined by the
Redox OS board of directors.

You can donate to Redox OS the following ways:

 - [Patreon](https://www.patreon.com/redox_os)
 - [Donorbox](https://donorbox.org/redox-os)
 - For donations larger than $1000, or for more donation options, please contact
   donate@redox-os.org

## Jeremy Soller

Jeremy Soller, is the creator, maintainer, and lead developer of Redox OS.
Donations to Jeremy Soller are treated as a taxable gift, and will be used at
his discretion.

You can donate to Jeremy Soller the following ways:

- [Liberapay](https://liberapay.com/redox_os)
- [Paypal](https://www.paypal.me/redoxos)
- Jeremy Soller no longer accepts Bitcoin or Ethereum donations. Do not send
  anything to the previously listed addresses, as it will not be received.

The following patrons have donated $4 or more to Jeremy Soller for use in developing Redox OS:
{{% partial "donors/jackpot51.html" %}}
