+++
title = "Quickstart"
+++

- [Latest Release](https://www.redox-os.org/news/release-0.8.0/)
- [Getting Started (Redox Book)](https://doc.redox-os.org/book/ch02-00-getting-started.html)
- [Redox Images](https://static.redox-os.org/img/)
- [Releases](https://gitlab.redox-os.org/redox-os/redox/-/releases)

## Building Redox

- Building Redox from source code is supported on Linux, and may work on some other platforms, but is not currently supported on Windows. To build Redox, please follow the instructions in the [Redox Book](https://doc.redox-os.org/book/ch02-06-podman-build.html). If you are on Pop!_OS, Ubuntu or Debian, you can follow [these instructions](https://doc.redox-os.org/book/ch02-05-building-redox.html).
