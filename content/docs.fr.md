+++
title = "Documentation"
+++

## Départ

Il est fortement recommandé de commencer par le
[livre](https://doc.redox-os.org/book/), qui explique comment configurer
et utiliser Redox.

## Références

[Livre](https://doc.redox-os.org/book/). Un livre qui présente l'architecture de Redox.

[Conférences](/talks/). Conférences de Redox données à divers événements.

[redox_syscall](https://docs.rs/redox_syscall/latest/syscall/). Documentation for the Redox system calls.

[libstd](https://doc.rust-lang.org/stable/std/). Documentation for the Rust standard library.

[Manuel de Ion](https://doc.redox-os.org/ion-manual/). Documentation du shell de Ion.

## Contribuer à Redox

- Lisez [CONTRIBUTING.md](https://gitlab.redox-os.org/redox-os/redox/-/blob/master/CONTRIBUTING.md)

Vous pouvez contribuer à la documentation / au code de Redox dans ces dépôts:

(Ils sont ordonnés par difficulté, les premiers étant les plus simples)

- [Site](https://gitlab.redox-os.org/redox-os/website)
- [Livre](https://gitlab.redox-os.org/redox-os/book)
- [Dépôt principal (construction et configuration du système)](https://gitlab.redox-os.org/redox-os/redox)
- [Paquets Redox (tous les composants systèmes / logiciels portés)](https://gitlab.redox-os.org/redox-os/cookbook)
- [L'environnement de bureau Redox](https://gitlab.redox-os.org/redox-os/orbital)
- [Bibliothèque client d'Orbital](https://gitlab.redox-os.org/redox-os/orbclient)
- [Applications d'Orbital](https://gitlab.redox-os.org/redox-os/orbutils)
- [Bibliothèque C de Redox](https://gitlab.redox-os.org/redox-os/relibc)
- [Pilotes](https://gitlab.redox-os.org/redox-os/drivers)
- [Noyau](https://gitlab.redox-os.org/redox-os/kernel)

## Communication et Chat

Veuillez voir sur la [Communauté](/fr/community/)


## Conduite

Nous appliquons le [code de conduite de Rust](https://www.rust-lang.org/policies/code-of-conduct).
