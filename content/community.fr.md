+++
title = "Communauté"
+++

Cette page tente d'expliquer comment la communauté Redox OS est organisée et vous aide à y naviguer.


<a id="chat"></a>
## [Chat](https://matrix.to/#/#redox:matrix.org)

Matrix est actuellement en cours d'évaluation pour être utilisé comme salon de discussion officiel de Redox OS.

<a id="chat"></a>
## [Mattermost](https://chat.redox-os.org)

Jusqu'à récemment, nous utilisions Mattermost comme serveur de discussion, que nous conservons actuellement à des fins d'archivage. Contactez-nous sur Redox OS/General si vous avez besoin d'accéder au serveur Mattermost.


<a id="gitlab"></a>
## [GitLab](https://gitlab.redox-os.org/redox-os/redox)

Un moyen de communication un peu plus formel avec les autres développeurs de Redox, mais un peu moins rapide et pratique que le chat. Soumettez une Issue lorsque vous rencontrez des problèmes de compilation, de tests ou si vous souhaitez simplement discuter d'un sujet en particulier, qu'il s'agisse de fonctionnalités, de style de code, d'incohérences de code, de modifications et de correctifs mineurs, etc.


<a id="forum"></a>
## [Forum](https://discourse.redox-os.org/)

C'est la meilleure façon de discuter de sujets plus généraux qui ne concernent pas des choses spécifiques. Vous pouvez vous inscrire comme sur n'importe quel autre site Web.

<a id="reddit"></a>
## [Redox OS on Reddit](https://www.reddit.com/r/Redox/)

Si vous voulez un aperçu rapide de ce qui se passe et en parler.

[reddit.com/r/rust](https://www.reddit.com/r/rust) pour des nouvelles et des discussions sur Rust.

<a id="mastodon"></a>
## [Mastodon](https://fosstodon.org/@redox)

Nouvelles et actualités liées.
