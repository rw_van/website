+++
title = "Darowizny"
+++

## Jackpot51

Jackpot51, albo Jeremy Soller, jest twórcą, opiekunem i głównym developerem systemu Redox OS.

Możesz przekazać mu datek pieniężny następującymi środkami:

- [Patreon](https://www.patreon.com/redox_os)
- [Liberapay](https://liberapay.com/redox_os)
- [Paypal](https://www.paypal.me/redoxos)
- Jeremy Soller no longer accepts Bitcoin or Ethereum donations. Do not send
  anything to the previously listed addresses, as it will not be received.

Następujące osoby podarowały $4 lub więcej na wsparcie rozwoju Redox OS:
{{% partial "donors/jackpot51.html" %}}
