+++
title = "Início"
url = "/home"
+++
<div class="row install-row">
  <div class="col-md-8">
    <p class="pitch">
      O <b>Redox</b> é um sistema operacional Unix-like escrito em <a style="color: inherit;" href="https://www.rust-lang.org/"><b>Rust</b></a>,
      que busca trazer as inovações desta linguagem de programação para um microkernel moderno e um conjunto completo de aplicações.
    </p>
  </div>
  <div class="col-md-4 install-box">
    <br/>
    <a class="btn btn-primary" href="/pt/quickstart/">Começo Rápido</a>
    <a class="btn btn-default" href="https://gitlab.redox-os.org/redox-os/redox/">GitLab</a>
  </div>
</div>
<div class="row features">
  <div class="col-md-6">
    <ul class="laundry-list" style="margin-bottom: 0px;">
      <li>Inspirado pelo Plan 9, Minix, BSD e Linux</li>
      <li>Implementado em Rust</li>
      <li>Design de Microkernel</li>
      <li>Inclui uma GUI opcional - Orbital</li>
      <li>Suporta a biblioteca padrão da Rust</li>
    </ul>
  </div>
  <div class="col-md-6">
    <ul class="laundry-list">
      <li>Licença MIT</li>
      <li>Os drivers são executados no espaço do usuário</li>
      <li>Inclui os comandos Unix mais comuns</li>
      <li>Nova biblioteca para portar programas em C (relibc)</li>
      <li>Veja <a href="/pt/screens/">Redox em Ação</a></li>
    </ul>
  </div>
</div>
<div class="row features">
  <div class="col-sm-12">
    <div style="font-size: 16px; text-align: center;">
      Redox executando Orbital
    </div>
    <a href="/img/redox-orbital/large.png">
      <picture>
        <source media="(min-width: 1300px)" srcset="/img/redox-orbital/large.webp" type="image/webp">
        <source media="(min-width: 640px)" srcset="/img/redox-orbital/medium.webp" type="image/webp">
        <source media="(min-width: 320px)" srcset="/img/redox-orbital/medium.webp" type="image/webp">
        <source media="(min-width: 1300px)" srcset="/img/redox-orbital/large.png" type="image/png">
        <source media="(min-width: 640px)" srcset="/img/redox-orbital/medium.png" type="image/png">
        <source media="(min-width: 320px)" srcset="/img/redox-orbital/small.png" type="image/png">
        <img src="/img/redox-orbital/medium.png" class="img-responsive" alt="Redox and Orbital">
      </picture>
    </a>
  </div>
</div>
